# coding: utf-8
# !/usr/bin/python

"""
Project: simulator
Wed Jul 13 23:19:53 2016
"""

# Author
__author__ = 'Jason Xing Zhang'
__email__ = 'xingz@uvic.ca'
__major__ = 1
__minor__ = 0
__revision__ = 3
__timestamp__ = '2016-06-16 | 14:10'

# Version
__version_info__ = (__major__, __minor__, __revision__)
__version__ = '{0}.{1}.{2}'.format(*__version_info__)

